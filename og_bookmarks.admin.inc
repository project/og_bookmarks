<?php

/**
 * Menu callback; Build the menu link editing form.
 */
function og_bookmarks_edit_item(&$form_state, $type, $item, $group) {
  global $user;

	$menu['menu_name'] = 'og_bookmarks-' . $group->nid;
	$form_state['og_bookmarks']['groupid'] = $group->nid;

	if ((!(og_is_group_admin($group) && user_access('administer group og_bookmarks')))) {
		drupal_set_message('You do not have permission to edit this menu.','error');
		drupal_goto($path);
		return;
	}

  $form['og_bookmarks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu settings'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#weight' => -2,
    '#attributes' => array('class' => 'menu-item-form'),
    '#item' => $item,
  );

  if ($type == 'addpage') {
  	$path = array();
  	$numargs = func_num_args();
  	$arg_list = func_get_args();
      for ($i = 4; $i < $numargs; $i++) {
  		$path[] = $arg_list[$i];
      }
  	$path = join('/', $path);
    // This is an add this page form, initialize the menu link.
    $item = array('link_title' => '', 'mlid' => 0, 'plid' => 0, 'menu_name' => 'og_bookmarks-' . $group->nid, 'weight' => 0, 'link_path' => '', 'options' => array(), 'module' => 'og_bookmarks', 'expanded' => 0, 'hidden' => 0, 'has_children' => 0, 'newwindow' => 0);
    $item['link_path']=$path;
    $menu_item = menu_get_item($item['link_path']);
    $item['link_title'] = $menu_item['title'];
  } else {
    $path = $item['link_path'];
  }
  
  foreach (array('link_path', 'mlid', 'module', 'has_children', 'options') as $key) {
    $form['og_bookmarks'][$key] = array('#type' => 'value', '#value' => $item[$key]);
  }
  // Any item created or edited via this interface is considered "customized".
  $form['og_bookmarks']['customized'] = array('#type' => 'value', '#value' => 1);
  $form['og_bookmarks']['original_item'] = array('#type' => 'value', '#value' => $item);

  if (isset($item['options']['query'])) {
    $path .= '?'. $item['options']['query'];
  }
  if (isset($item['options']['fragment'])) {
    $path .= '#'. $item['options']['fragment'];
  }
  if ($item['module'] == 'og_bookmarks') {
    $form['og_bookmarks']['link_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#default_value' => $path,
      '#description' => t('The path this bookmark links to. This can be an internal Drupal path such as %add-node or an external URL such as %drupal. Enter %front to link to the front page. Enter %folder to make this item a folder.', array('%front' => '<front>', '%add-node' => 'node/add', '%drupal' => 'http://drupal.org', '%folder' => '<folder>')),
      '#required' => TRUE,
    );
  }
  else {
    $form['og_bookmarks']['_path'] = array(
      '#type' => 'item',
      '#title' => t('Path'),
      '#description' => l($item['link_title'], $item['href'], $item['options']),
    );
  }
  $form['og_bookmarks']['link_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu link title'),
    '#default_value' => $item['link_title'],
    '#description' => t('The link text corresponding to this item that should appear in the menu.'),
    '#required' => TRUE,
  );
  $form['og_bookmarks']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($item['options']['attributes']['title']) ? $item['options']['attributes']['title'] : '',
    '#rows' => 1,
    '#description' => t('The description displayed when hovering over a bookmark.'),
  );
  $form['og_bookmarks']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => !$item['hidden'],
    '#description' => t('Bookmarks that are not enabled will not be listed anywhere.'),
  );
  $form['og_bookmarks']['expanded'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expanded'),
    '#default_value' => $item['expanded'],
    '#description' => t('If selected and this bookmark has children, the bookmark will always appear expanded.'),
  );

  // Generate a list of possible parents (not including this item or descendants).
  $options = menu_parent_options(og_bookmarks_get_menus($group), $item);
  $default = $item['menu_name'] .':'. $item['plid'];
  $menu_groupid = (str_replace('og_bookmarks-', '', $item['menu_name']));
  switch ($item['menu_name']) {
    case $group->nid:
      $options[$item['menu_name'] . ':0'] = '<' . $group->title . '>';
      break;
    case 'og_bookmarks-defaults':
      $options[$item['menu_name'] . ':0'] = '<Defaults>';
      break;
    case 'og_bookmarks-presets':
      $options[$item['menu_name'] . ':0'] = '<Presets>';
      break;
    default:
      $options[$item['menu_name'] . ':0'] = '<' . $group->title . '>';
      break;
  }
  $form['og_bookmarks']['parent'] = array(
    '#type' => 'select',
    '#title' => t('Parent item'),
    '#default_value' => $default,
    '#options' => $options,
    '#description' => t('The maximum depth for an item and all its children is fixed at !maxdepth. Some og_bookmarks may not be available as parents if selecting them would exceed this limit.', array('!maxdepth' => MENU_MAX_DEPTH)),
    '#attributes' => array('class' => 'menu-title-select'),
  );
  $form['og_bookmarks']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => $item['weight'],
    '#description' => t('Optional. In the list, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Validate form values for a menu link being added or edited.
 */
function og_bookmarks_edit_item_validate($form, &$form_state) {
  $item = &$form_state['values']['og_bookmarks'];
  if ($item['link_path']=='<folder>') {
    $item['link_path']='folder';
  }
  else {
    $normal_path = drupal_get_normal_path($item['link_path']);
    if ($item['link_path'] != $normal_path) {
      drupal_set_message(t('The menu system stores system paths only, but will use the URL alias for display. %link_path has been stored as %normal_path', array('%link_path' => $item['link_path'], '%normal_path' => $normal_path)));
      $item['link_path'] = $normal_path;
    }
    if (!menu_path_is_external($item['link_path'])) {
      $parsed_link = parse_url($item['link_path']);
      if (isset($parsed_link['query'])) {
        $item['options']['query'] = $parsed_link['query'];
      }
      if (isset($parsed_link['fragment'])) {
        $item['options']['fragment'] = $parsed_link['fragment'];
      }
      if ($item['link_path'] != $parsed_link['path']) {
        $item['link_path'] = $parsed_link['path'];
      }
    }
    if (!trim($item['link_path']) || !menu_valid_path($item)) {
      form_set_error('link_path', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $item['link_path'])));
    }
  }
}

/**
 * Process menu and menu item add/edit form submissions.
 */
function og_bookmarks_edit_item_submit($form, &$form_state) {
  global $user;
  $item = $form_state['values']['og_bookmarks'];

  // The value of "hidden" is the opposite of the value
  // supplied by the "enabled" checkbox.
  $item['hidden'] = (int) !$item['enabled'];
  unset($item['enabled']);

  $item['options']['attributes']['title'] = $item['description'];

  list($item['menu_name'], $item['plid']) = explode(':', $item['parent']);
  if (!menu_link_save($item)) {
    drupal_set_message(t('There was an error saving the menu link.'), 'error');
  }
  //If they are editing their menu, they should be doing so with the My Menu link.
  if (user_access('administer group og_bookmarks') && $item['menu_name']=='og_bookmarks-'. $form_state['og_bookmarks']['groupid']) {
    $form_state['redirect'] = 'node/' . $form_state['og_bookmarks']['groupid'];
  }
}


/**
 * Form for editing an entire menu tree at once.
 *
 * Shows for one menu the menu items accessible to the current user and
 * relevant operations.
 */
function og_bookmarks_overview_form(&$form_state, $group) {
  global $_menu_admin;

  $sql = "
    SELECT m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.title, m.title_callback, m.title_arguments, m.type, m.description, ml.*
    FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
    WHERE ml.menu_name = '%s'
    ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";

  $menu['menu_name']='og_bookmarks-'. $group->nid;
  $result = db_query($sql, $menu['menu_name']);
  $tree = menu_tree_data($result);
  $node_links = array();
  menu_tree_collect_node_links($tree, $node_links);
  // We indicate that a menu administrator is running the menu access check.
  $_menu_admin = TRUE;
  menu_tree_check_access($tree, $node_links);
  $_menu_admin = FALSE;

  $form = _og_bookmarks_overview_tree_form($tree, $menu, $group);
  $form['#menu'] =  $menu;

  if (element_children($form)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }  elseif (!element_children($form)) {
    $form['empty_menu'] = array('#value' => t('There are no og_bookmarks yet.'));
  }
  return $form;
}

/**
 * Recursive helper function for menu_overview_form().
 */
function _og_bookmarks_overview_tree_form($tree, $menu, $group) {
  global $user;
  static $form = array('#tree' => TRUE);
  $actionpaths = 'admin/build/og_bookmarks-customize/';
  if (user_access('administer group og_bookmarks') && $menu['menu_name']=='og_bookmarks-'. $group->nid) {
    $actionpaths = 'og_bookmarks/';
  }
  foreach ($tree as $data) {
    $title = '';
    $item = $data['link'];
    // Don't show callbacks; these have $item['hidden'] < 0.
    if ($item && $item['hidden'] >= 0) {
      $mlid = 'mlid:'. $item['mlid'];
      $form[$mlid]['#item'] = $item;
      $form[$mlid]['#attributes'] = $item['hidden'] ? array('class' => 'menu-disabled') : array('class' => 'menu-enabled');
      if ($item['href']=='folder') {
        $form[$mlid]['title']['#value'] = $item['title'] . ($item['hidden'] ? ' ('. t('disabled') .')' : '');
      }
      else {
        $form[$mlid]['title']['#value'] = l($item['title'], $item['href'], $item['localized_options']) . ($item['hidden'] ? ' ('. t('disabled') .')' : '');
      }
      $form[$mlid]['mlid'] = array(
        '#type' => 'hidden',
        '#value' => $item['mlid'],
      );
      $form[$mlid]['plid'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($form_state[$mlid]['plid']) ? $form_state[$mlid]['plid'] : $item['plid'],
        '#size' => 6,
      );
      $form[$mlid]['hidden'] = array(
        '#type' => 'checkbox',
        '#default_value' => !$item['hidden'],
      );
      $form[$mlid]['expanded'] = array(
        '#type' => 'checkbox',
        '#default_value' => $item['expanded'],
      );
      $form[$mlid]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 50,
        '#default_value' => isset($form_state[$mlid]['weight']) ? $form_state[$mlid]['weight'] : $item['weight'],
      );
      // Build a list of operations.
      $operations = array();
      $operations['edit'] = l(t('edit'), $actionpaths . $group->nid . '/item/edit/'. $item['mlid']);
      // Only items created by the og_bookmarks module can be deleted.
      if ($item['module'] == 'og_bookmarks' || $item['updated'] == 1) {
        $operations['delete'] = l(t('delete'), $actionpaths . $group->nid . '/item/delete/'. $item['mlid']);

        $form[$mlid]['operations'] = array();
        foreach ($operations as $op => $value) {
          $form[$mlid]['operations'][$op] = array('#value' => $value);
        }
      }
      else {
        // Build a list of operations.
        $operations = array();

        $form[$mlid]['operations'] = array();
        foreach ($operations as $op => $value) {
          $form[$mlid]['operations'][$op] = array('#value' => $value);
        }
      }
    }

    if ($data['below']) {
      _og_bookmarks_overview_tree_form($data['below'], $menu, $group);
    }
  }
  return $form;
}

/**
 * Submit handler for the menu overview form.
 *
 * This function takes great care in saving parent items first, then items
 * underneath them. Saving items in the incorrect order can break the menu tree.
 *
 * @see og_bookmarks_overview_form()
 */
function og_bookmarks_overview_form_submit($form, &$form_state) {
  // When dealing with saving menu items, the order in which these items are
  // saved is critical. If a changed child item is saved before its parent,
  // the child item could be saved with an invalid path past its immediate
  // parent. To prevent this, save items in the form in the same order they
  // are sent by $_POST, ensuring parents are saved first, then their children.
  // See http://drupal.org/node/181126#comment-632270
  $order = array_flip(array_keys($form['#post'])); // Get the $_POST order.
  $form = array_merge($order, $form); // Update our original form with the new order.

  $updated_items = array();
  $fields = array('expanded', 'weight', 'plid');
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['#item'])) {
      $element = $form[$mlid];
      // Update any fields that have changed in this menu item.
      foreach ($fields as $field) {
        if ($element[$field]['#value'] != $element[$field]['#default_value']) {
          $element['#item'][$field] = $element[$field]['#value'];
          $updated_items[$mlid] = $element['#item'];
        }
      }
      if (module_exists('og_bookmarks_public')) {
        // Public is a special case, the value needs to be inserted into the attributes array
        if ($element['public']['#value'] != $element['public']['#default_value']) {
          $element['#item']['options']['attributes']['public'] = $element['public']['#value'];
          $updated_items[$mlid] = $element['#item'];
        }
      }
      // Hidden is a special case, the value needs to be reversed.
      if ($element['hidden']['#value'] != $element['hidden']['#default_value']) {
        $element['#item']['hidden'] = !$element['hidden']['#value'];
        $updated_items[$mlid] = $element['#item'];
      }
    }
  }

  // Save all our changed items to the database.
  foreach ($updated_items as $item) {
    $item['customized'] = 1;
    menu_link_save($item);
  }
}

/**
 * Theme the menu overview form into a table.
 *
 * @ingroup themeable
 */
function theme_og_bookmarks_overview_form($form) {
  $output = '';
  drupal_add_tabledrag('og_bookmarks-overview', 'match', 'parent', 'menu-plid', 'menu-plid', 'menu-mlid', TRUE, MENU_MAX_DEPTH - 1);
  drupal_add_tabledrag('og_bookmarks-overview', 'order', 'sibling', 'menu-weight');

  if (module_exists('og_bookmarks_public')) {
    $header = array(
      t('Menu item'),
      array('data' => t('Enabled'), 'class' => 'checkbox'),
      array('data' => t('Expanded'), 'class' => 'checkbox'),
      array('data' => t('Public'), 'class' => 'checkbox'),
      t('Weight'),
      array('data' => t('Operations'), 'colspan' => '3'),
    );
  }
  else {
    $header = array(
      t('Menu item'),
      array('data' => t('Enabled'), 'class' => 'checkbox'),
      array('data' => t('Expanded'), 'class' => 'checkbox'),
      t('Weight'),
      array('data' => t('Operations'), 'colspan' => '3'),
    );
  }

  $rows = array();
  foreach (element_children($form) as $mlid) {
    if (isset($form[$mlid]['hidden'])) {
      $element = &$form[$mlid];
      // Build a list of operations.
      $operations = array();
      foreach (element_children($element['operations']) as $op) {
        $operations[] = drupal_render($element['operations'][$op]);
      }
      while (count($operations) < 2) {
        $operations[] = '';
      }

      // Add special classes to be used for tabledrag.js.
      $element['plid']['#attributes']['class'] = 'menu-plid';
      $element['mlid']['#attributes']['class'] = 'menu-mlid';
      $element['weight']['#attributes']['class'] = 'menu-weight';

      // Change the parent field to a hidden. This allows any value but hides the field.
      $element['plid']['#type'] = 'hidden';

      $row = array();
      $row[] = theme('indentation', $element['#item']['depth'] - 1) . drupal_render($element['title']);
      $row[] = array('data' => drupal_render($element['hidden']), 'class' => 'checkbox');
      $row[] = array('data' => drupal_render($element['expanded']), 'class' => 'checkbox');
      if (module_exists('og_bookmarks_public')) {
        $row[] = array('data' => drupal_render($element['public']), 'class' => 'checkbox');
      }

      $row[] = drupal_render($element['weight']) . drupal_render($element['plid']) . drupal_render($element['mlid']);
      $row = array_merge($row, $operations);

      $row = array_merge(array('data' => $row), $element['#attributes']);
      $row['class'] = !empty($row['class']) ? $row['class'] .' draggable' : 'draggable';
      $rows[] = $row;
    }
  }
  if ($rows) {
    $output .= theme('table', $header, $rows, array('id' => 'og_bookmarks-overview'));
  }
  $output .= drupal_render($form);

  return $output;
}

/**
 * Menu callback; Check access and present a confirm form for deleting a menu link.
 */
function og_bookmarks_item_delete_page($item) {
  // Links defined via hook_menu may not be deleted. Updated items are an
  // exception, as they can be broken.
  if ($item['module'] == 'system' && !$item['updated']) {
    drupal_access_denied();
    return;
  }
  return drupal_get_form('og_bookmarks_item_delete_form', $item);
}

/**
 * Build a confirm form for deletion of a single menu link.
 */
function og_bookmarks_item_delete_form(&$form_state, $item) {
	$matches = array();
	if (preg_match('/^og_bookmarks\/(\d+)/', drupal_get_normal_path(drupal_get_path_alias($_GET['q'])), $matches)) {
		$group = node_load($matches[1]);
	}
	if ((!(og_is_group_admin($group) && user_access('administer group og_bookmarks')))) {
		drupal_set_message('You do not have permission to edit this menu.','error');
#		drupal_goto($path);
		return;
	}

  $form['#item'] = $item;
  return confirm_form($form, t('Are you sure you want to delete the bookmark %item?', array('%item' => $item['link_title'])), 'og_bookmarks/' . $group->nid . '/edit');
}

/**
 * Process menu delete form submissions.
 */
function og_bookmarks_item_delete_form_submit($form, &$form_state) {
	if (preg_match('/^og_bookmarks\/(\d+)/', drupal_get_normal_path(drupal_get_path_alias($_GET['q'])), $matches)) {
		$group = node_load($matches[1]);
	}
  $item = $form['#item'];
  menu_link_delete($item['mlid']);
  $t_args = array('%title' => $item['link_title']);
  drupal_set_message(t('The bookmark %title has been deleted.', $t_args));
  watchdog('menu', 'Deleted bookmark %title.', $t_args, WATCHDOG_NOTICE);
  $form_state['redirect'] = 'og_bookmarks/' . $group->nid . '/edit';
}